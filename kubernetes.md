# kubernetes notes

# Minikube
1. Create minikube cluster
    ```bash
    minikube start --driver=virtualbox --cpus=6 --memory=4096MB
    ```
1. Set local docker context to minikube
    ```bash
    eval $(minikube -p minikube docker-env)
    ```
1. Get minikube IP address
    ```bash
    minikube ip
    ```
