# docker notes

## Portainer
```bash
docker pull portainer/portainer
```

```bash
docker volume create portainer
```

```bash
docker create --name portainer \
    -p 9000:9000 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer:/data \
    portainer/portainer \
    --no-auth
```

```bash
docker start portainer
```

## Nexus OSS
```bash
docker pull sonatype/nexus3
```

```bash
docker volume create nexus-data
```

```bash
docker create --name nexus \
    -p 8081:8081 \
    -v nexus-data:/nexus-data \
    sonatype/nexus3
```

```bash
docker start nexus
```

```bash
docker exec -it nexus /bin/bash -c "cat /nexus-data/admin.password"
```

## SonarQube
```bash
docker pull sonarqube:8-community
```

```bash
docker volume create sonarqube
```

```bash
docker create --name sonarqube \
    -p 9000:9000 \
    -v sonarqube:/opt/sonarqube \
    sonarqube:8-community
```

```bash
docker start sonarqube
```

## Grafana
```bash
docker pull grafana/grafana
```

```bash
docker volume create grafana
```

```bash
docker create --name grafana \
    -p 3000:3000 \
    -v grafana:/var/lib/grafana \
    grafana/grafana
```

```bash
docker start grafana
```

## Prometheus
```bash
docker pull prom/prometheus
```

```bash
docker volume create prometheus
```

```bash
docker create --name prometheus \
    -p 9090:9090 \
    -v prometheus:/etc/prometheus \
    prom/prometheus
```

```bash
docker start prometheus
```
